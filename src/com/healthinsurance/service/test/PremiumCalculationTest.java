package com.healthinsurance.service.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.healthinsurance.model.Customer;
import com.healthinsurance.service.PremiumCalculation;
import com.healthinsurance.utils.gender;

public class PremiumCalculationTest {
	
	PremiumCalculation pc;
	Customer customer;
	@Before
	public void setUp() {
		pc = new PremiumCalculation();
		customer= new Customer();
		
	}


	
	@Test
	public void testPremiumMaleWithgoodHabits() {
		PremiumCalculation pc = new PremiumCalculation();
		Map<String,String> currentHealth= new HashMap<String,String>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood sugar", "No");
		currentHealth.put("Overweight", "Yes");
		Map<String,String> habit= new HashMap<String,String>();
		habit.put("Smoking", "No");
		habit.put("Alcohol", "Yes");
		habit.put("Daily exercise", "Yes");
		habit.put("Drugs", "No");
		customer.setName("Robert");
		customer.setAge(34);
		customer.setHabits(habit);
		customer.setCurrentHealth(currentHealth);
		customer.setGn(gender.Male);
		int pre=pc.calculatePremium(customer);
		System.out.println("premium="+pre);
		Assert.assertEquals(6855, pre);
		Assert.assertNotNull(pre);
	}
	
	@Test
	public void testPremiumMalegoodbadHabits() {
		Map<String,String> currentHealth= new HashMap<String,String>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood sugar", "No");
		currentHealth.put("Overweight", "Yes");
		Map<String,String> habit= new HashMap<String,String>();
		habit.put("Smoking", "No");
		habit.put("Alcohol", "No");
		habit.put("Daily exercise", "Yes");
		habit.put("Drugs", "Yes");
		customer.setName("Rufat");
		customer.setAge(34);
		customer.setHabits(habit);
		customer.setCurrentHealth(currentHealth);
		customer.setGn(gender.Male);
		int pre=pc.calculatePremium(customer);
		System.out.println("premium="+pre);
		Assert.assertNotNull(pre);
	}
	@Test
	public void testPremiumMaleWithBadHabits() {
		PremiumCalculation pc = new PremiumCalculation();
		Map<String,String> currentHealth= new HashMap<String,String>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood sugar", "No");
		currentHealth.put("Overweight", "Yes");
		Map<String,String> habit= new HashMap<String,String>();
		habit.put("Smoking", "No");
		habit.put("Alcohol", "No");
		habit.put("Daily exercise", "No");
		habit.put("Drugs", "Yes");
		customer.setName("Arnold");
		customer.setAge(24);
		customer.setHabits(habit);
		customer.setCurrentHealth(currentHealth);
		customer.setGn(gender.Male);
		int pre=pc.calculatePremium(customer);
		System.out.println("premium="+pre);
		Assert.assertNotNull(pre);
	}
	
	@Test
	public void testPremiumFemaleWithGoodBadHabits() {
		PremiumCalculation pc = new PremiumCalculation();
		Map<String,String> currentHealth= new HashMap<String,String>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood sugar", "No");
		currentHealth.put("Overweight", "Yes");
		Map<String,String> habit= new HashMap<String,String>();
		habit.put("Smoking", "No");
		habit.put("Alcohol", "No");
		habit.put("Daily exercise", "Yes");
		habit.put("Drugs", "No");
		customer.setName("Anzlina");
		customer.setAge(24);
		customer.setHabits(habit);
		customer.setCurrentHealth(currentHealth);
		customer.setGn(gender.Female);
		int pre=pc.calculatePremium(customer);
		System.out.println("premium= female::"+pre);
		Assert.assertEquals(pre, 5389);
		Assert.assertNotNull(pre);
	}
	
	@After
	public void end() {
		pc = null;
	}

}
