package com.healthinsurance.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.healthinsurance.model.Customer;
import com.healthinsurance.utils.gender;



public class PremiumCalculation {
	public static final int basePrice=5000;

/*	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		PremiumCalculation pc = new PremiumCalculation();
		Customer customer = new Customer();
		Map<String,String> currentHealth= new HashMap<String,String>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood sugar", "No");
		currentHealth.put("Overweight", "Yes");
		Map<String,String> habit= new HashMap<String,String>();
		habit.put("Smoking", "No");
		habit.put("Alcohol", "No");
		habit.put("Daily exercise", "Yes");
		habit.put("Drugs", "Yes");
		customer.setName("Rufat");
		customer.setAge(24);
		customer.setHabits(habit);
		customer.setCurrentHealth(currentHealth);
		customer.setGn(gender.Male);
		int pre=pc.calculatePremium(customer);
		System.out.println("premium="+pre);
		

	}*/

	
	public int calculatePremium(Customer customer) {
		
		System.out.println("Name::"+customer.getName());
		int priAmt=basePrice;
		
        priAmt=calculatePremiumOnAge(customer.getAge(), priAmt);
		
		if(customer.getGn().equals(gender.Male)) {
			priAmt = priAmt + (priAmt * 2/100);
		}
       
		priAmt=calculatePremiumOnCurrentHealth(priAmt, customer.getCurrentHealth());

		priAmt=calculatePremiumOnHabit(priAmt, customer.getHabits());
		
		return priAmt;
	}
	
	private int calculatePremiumOnAge(int age,int priAmt) {
		if (age > 18 && age<=25) {
			priAmt = priAmt + (priAmt * 10/100);
		}else if (age>25 && age <= 30) {
			priAmt = priAmt + (priAmt * 10/100);
			priAmt = priAmt + (priAmt * 10/100);
		}else if (age>30 && age <= 35) {
			priAmt = priAmt + (priAmt * 10/100);
			priAmt = priAmt + (priAmt * 10/100);
			priAmt = priAmt + (priAmt * 10/100);
		}else if (age>35 && age <= 40) {
			priAmt = priAmt + (priAmt * 10/100);
			priAmt = priAmt + (priAmt * 10/100);
			priAmt = priAmt + (priAmt * 10/100);
		}else if(age > 40) {
			for (int i=41; i<=age;i+=5) {
				priAmt = priAmt + (priAmt * 10/100);
			}
		}
		
		return priAmt;
	}
	
	
	private int calculatePremiumOnCurrentHealth(int priAmt,Map<String,String> currentHealth) {
		boolean currHealth=false;
		int count=0;
		Iterator<Entry<String, String>> it=currentHealth.entrySet().iterator();
		while(it.hasNext()) {
			Entry<String, String> entry =(Entry<String, String>) it.next();
			if(entry.getValue().equalsIgnoreCase("Yes")) {
				currHealth=true;
				count++;
				break;
			}
		}
		
		if(currHealth) {
			priAmt = priAmt + (priAmt * count/100);
		}
		return priAmt;
	}
	
	
	private int calculatePremiumOnHabit(int priAmt,Map<String,String> habit) {
		boolean badHabits=false,goodHabits =false;
		
		Iterator<Entry<String, String>> it=habit.entrySet().iterator();
		while(it.hasNext()) {
			Entry<String, String> entry =(Entry<String, String>) it.next();
			if(entry.getKey().equals("Daily exercise") && entry.getValue().equalsIgnoreCase("Yes")) {
				goodHabits=true;
			}
			if((!entry.getKey().equals("Daily exercise")) && entry.getValue().equalsIgnoreCase("Yes")) {
				badHabits=true;
			}
		}
		
		if(!(goodHabits && badHabits)) {
			if(goodHabits) {
			priAmt = priAmt - (priAmt * 3/100);
			}
			if(badHabits) {
			priAmt = priAmt + (priAmt * 3/100);
			}
		}
		
		return priAmt;
	}
}
