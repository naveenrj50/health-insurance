package com.healthinsurance.model;

import java.util.Map;
import com.healthinsurance.utils.*;

public class Customer {

	String name;
	int age;
	Map<String,String> currentHealth;
	Map<String,String> habits;
	gender gn;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Map<String, String> getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(Map<String, String> currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Map<String, String> getHabits() {
		return habits;
	}
	public void setHabits(Map<String, String> habits) {
		this.habits = habits;
	}
	public gender getGn() {
		return gn;
	}
	public void setGn(gender gn) {
		this.gn = gn;
	}
	
}
